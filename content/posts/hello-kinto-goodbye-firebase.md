---
layout: post
title:  "Goodbye Firebase, Hello Kinto"
date:   2018-11-01
comments: true
categories: [Firebase, Databases, NoSql, Kinto]
---

I have been using [Ionic](https://ionicframework.com/) to build web apps for awhile now, and for the longest time, Firebase was the best way to go for any project, allowing you to get started securely with a simple NoSQL database, capable of Auth, server functions, and handling file storage.

But then I used it in production...
Firebase is great, but:

- The database is completly exposed to anyone with access to the project.

This wasn't the end of the world, it just meant writing our own admin panel, but it would have been nice to have a PG Admin like front end, with granular permissions for project members. 

- The data is stored in the clear?

I know, Google is 'safe' (in the sense that it hasn't had a major data breach), but what if we wanted additional security?

Not a dealbreaker.

And Firebase functions are awesome for doing things with your server. It was a joy to write front end code in the same language (JS) as the server code, and not touch any SQL.

But then I found [Kinto](http://docs.kinto-storage.org/en/stable/index.html), Mozilla's self-hosted (HTTP Based) NoSQL DB. And it comes with its own Admin Panel, permissions are a snap, there is file storage, and connecting oAuth providers is a snap. It sits atop of a Postgres DB, so you end up with the best of both worlds -- a traditional SQL server that can be deployed anywhere and replicated, and a NoSql adapter to make HTTP calls to. And, there is a Javascript Client, [Kinto.js](https://github.com/Kinto/kinto.js/) for your front end needs!

Check out the doc's. It's super easy to get started. I am definitely using this in every project moving forward, especially if there is info or business logic you dont want left in the clear on some Google server ;)







